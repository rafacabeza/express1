# Proyecto con Express.js

- Proyecto donde se muestra el uso de los siguientes elementos:
    - Middleware propio y de terceros (morgan)
    - Servir contenido estático (carpeta public) y como directorio virtual.
    - Usar un motor de plantillas de forma básica: hbs (handlebars)